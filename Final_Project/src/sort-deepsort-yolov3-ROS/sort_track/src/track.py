#!/usr/bin/env python

"""
Modified version of DEEP SORT to remove the added benefit of detected deep features
"""
import rospy
import numpy as np
from darknet_ros_msgs.msg import BoundingBoxes
from deep_sort.detection import Detection
from deep_sort import nn_matching
from deep_sort.tracker import Tracker
from deep_sort import generate_detections as gdet
from deep_sort import preprocessing as prep
from cv_bridge import CvBridge
import cv2
from sensor_msgs.msg import Image
from sort_track.msg import IntList
from collections import deque

detections = []
scores = []
classes = []
allowedClasses = set(['car', 'truck', 'bus', 'motorcycle'])
MIN_HEIGHT = 50
MIN_WIDTH = 50
pts = [deque(maxlen=30) for _ in range(9999)]

def get_parameters():
	"""
	Gets the necessary parameters from .yaml file
	Returns tuple
	"""
	camera_topic = rospy.get_param("~camera_topic")
	detection_topic = rospy.get_param("~detection_topic")
	tracker_topic = rospy.get_param('~tracker_topic')
	return (camera_topic, detection_topic, tracker_topic)


def callback_det(data):
	global detections
	global scores
	global classes
	detections = []
	scores = []
	classes = []
	for box in data.bounding_boxes:
		if box.Class in allowedClasses and box.xmax-box.xmin > 50 and box.ymax-box.ymin > 50:
			detections.append(np.array([box.xmin, box.ymin, box.xmax-box.xmin, box.ymax-box.ymin]))
			scores.append(float('%.2f' % box.probability))
			classes.append(box.Class)
	detections = np.array(detections)

	#Display Image
	bridge = CvBridge()
	cv_rgb = bridge.imgmsg_to_cv2(data.image, "bgr8")
	#Features and detections
	detections_new = [Detection(bbox, score, []) for bbox,score in
						zip(detections,scores)]
	# Run non-maxima suppression.
	boxes = np.array([d.tlwh for d in detections_new])
	scores_new = np.array([d.confidence for d in detections_new])
	indices = prep.non_max_suppression(boxes, 1.0 , scores_new)
	detections_new = [detections_new[i] for i in indices]
	# Call the tracker
	tracker.predict()
	tracker.update(detections_new)
	#Detecting bounding boxes
	for index, det in enumerate(detections_new):
		bbox = det.to_tlbr()
		cv2.rectangle(cv_rgb,(int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),(100,255,50), 1)
		cv2.putText(cv_rgb , classes[index], (int(bbox[0]), int(bbox[1])), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (100,255,50), lineType=cv2.LINE_AA)
	#Tracker bounding boxes
	for track in tracker.tracks:
		if not track.is_confirmed() or track.time_since_update > 1:
			continue
		bbox = track.to_tlbr()
		msg.data = [int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3]), track.track_id]
		cv2.rectangle(cv_rgb, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),(255,255,255), 1)
		cv2.putText(cv_rgb, str(track.track_id),(int(bbox[2]), int(bbox[1])),0, 5e-3 * 200, (255,255,255),1)

		# Track the center of the bounding box
		center = (int(((bbox[0])+(bbox[2]))/2),int(((bbox[1])+(bbox[3]))/2))
		pts[track.track_id].append(center)
		thickness = 5
		cv2.circle(cv_rgb,  (center), 1, (255,20,147), thickness)

	    #draw motion path
		for j in range(1, len(pts[track.track_id])):
			if pts[track.track_id][j - 1] is None or pts[track.track_id][j] is None:
				continue
			thickness = int(np.sqrt(64 / float(j + 1)) * 2)
			cv2.line(cv_rgb,(pts[track.track_id][j-1]), (pts[track.track_id][j]),(255,20,147),thickness)

	cv2.imshow("YOLO + SORT", cv_rgb)
	cv2.waitKey(3)
		

def main():
	global tracker
	global msg
	msg = IntList()
	max_cosine_distance = 0.2
	nn_budget = 100
	metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
	tracker = Tracker(metric)
	#Initialize ROS node
	rospy.init_node('sort_tracker', anonymous=True)
	rate = rospy.Rate(10)
	# Get the parameters
	(camera_topic, detection_topic, tracker_topic) = get_parameters()
	#Subscribe to darknet_ros to get BoundingBoxes from YOLOv3
	sub_detection = rospy.Subscriber(detection_topic, BoundingBoxes , callback_det)
	while not rospy.is_shutdown():
		#Publish results of object tracking
		pub_trackers = rospy.Publisher(tracker_topic, IntList, queue_size=10)
		print(msg)
		pub_trackers.publish(msg)
		rate.sleep()


if __name__ == '__main__':
	try :
		main()
	except rospy.ROSInterruptException:
		pass
