#!/usr/bin/env python

#-*-coding: utf-8-*-

'''
Author: Nathaniel Hanson
Date: 04/01/2021
File: image_stich.py

Purpose: Stitch together two images using homography
'''

# Future proofing python 2
from __future__ import nested_scopes
from __future__ import generators
from __future__ import division
from __future__ import absolute_import
from __future__ import with_statement
from __future__ import print_function
# Standard package imports
import rospy
import sys
import tf
import os
import tf2_ros
import cv2
import roslib
import imutils
import math
import traceback
import numpy as np
from sensor_msgs.msg import Image, CompressedImage
from geometry_msgs.msg import Pose, PointStamped, Point
from tf2_geometry_msgs import PoseStamped
from std_msgs.msg import String, Header
from cv_bridge import CvBridge, CvBridgeError
# Transformation
import tf
import tf2_ros
from tf.transformations import quaternion_from_euler, euler_from_quaternion
import geometry_msgs.msg

UPDATE_RATE = 33333333.333 # nanoseconds

class ImageStitch:
    def __init__(self):
        # Intialize empty stores for current image
        self.rgb_image_left = None
        self.rgb_image_right = None
        self.depth_image_left = None
        self.depth_image_right = None
        # setup subscribed topics
        # initialize ros/cv2 bridge
        self.bridge = CvBridge()
        self.rgb_sub_left = rospy.Subscriber("/cam1/color/image_raw/compressed", CompressedImage, self.receive_rgb_left)
        self.depth_sub_left = rospy.Subscriber("/d1_out", Image, self.receive_depth_left, queue_size=100)
        self.depth_sub_right= rospy.Subscriber("/d2_out", Image, self.receive_depth_right, queue_size=100)
        self.rgb_sub_right = rospy.Subscriber("/cam2/color/image_raw/compressed", CompressedImage, self.receive_rgb_right)
        self.joint_color_pub = rospy.Publisher('/color_joint', Image, queue_size=100)
        self.joint_depth_pub = rospy.Publisher('/depth_joint', Image, queue_size=100)
        #self.timer = rospy.Timer(rospy.Duration(0, UPDATE_RATE), self.display_images)
        self.timer = rospy.Timer(rospy.Duration(0, UPDATE_RATE), self.publish_joint_color)
        self.timer2 = rospy.Timer(rospy.Duration(0, UPDATE_RATE), self.publish_joint_depth)

        # Initialize the empty homography
        self.homography = None
        self.stich_modes = (cv2.Stitcher_PANORAMA, cv2.Stitcher_SCANS)

    def decode_depth(self, msg):
        # 'msg' as type CompressedImage
        #depth_fmt, compr_type = msg.format.split(';')
        # remove white space
        #depth_fmt = depth_fmt.strip()
        #compr_type = compr_type.strip()
        #print(compr_type)
        #print(depth_fmt)

        # remove header from raw data
        # depth_header_size = 12
        #raw_data = msg.data[depth_header_size:]

        depth_img_raw = self.bridge.imgmsg_to_cv2(msg, desired_encoding='passthrough')
        #cv2.imdecode(np.fromstring(msg.data, np.uint8), cv2.IMREAD_UNCHANGED)
        return depth_img_raw
        
    def receive_rgb_left(self, data):
        np_arr = np.fromstring(data.data, np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        try:
            self.rgb_image_left = image_np
        except Exception as e:
            print(e)
    
    def receive_rgb_right(self, data):
        np_arr = np.fromstring(data.data, np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        try:
            self.rgb_image_right = image_np
        except Exception as e:
            print(e)

    def receive_depth_left(self, msg):
        self.depth_image_left = self.decode_depth(msg)
    
    def receive_depth_right(self, msg):
        self.depth_image_right = self.decode_depth(msg)

    def calculate_homography(self):
        if self.rgb_image_left is None or self.rgb_image_right is None:
            #print('no images!')
            return
        # Initiate SIFT Detector
        sift = cv2.xfeatures2d.SIFT_create()

        # Convert images to grayscale
        left = cv2.cvtColor(self.rgb_image_left, cv2.COLOR_RGB2GRAY)
        right = cv2.cvtColor(self.rgb_image_right, cv2.COLOR_RGB2GRAY)

        # Find the keypoints and descriptiors
        kpLeft, desLeft = sift.detectAndCompute(left, None)
        kpRight, desRight = sift.detectAndCompute(right, None)

        # Find Top M matches of descriptors in images
        FLANN_INDEX_KDTREE = 1
        index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
        search_params = dict(checks = 50)
        flann = cv2.FlannBasedMatcher(index_params, search_params)
        matches = flann.knnMatch(desLeft,desRight,k=2)
        # store all the good matches as per Lowe's ratio test.
        good = []
        for m,n in matches:
            if m.distance < 0.7*n.distance:
                good.append(m)
        matches = np.asarray(good)
        # align images using homography transformation
        MIN_MATCHES = 4
        if len(matches) >= MIN_MATCHES:
            ptsLeft = np.float32([ kpLeft[m.queryIdx].pt for m in matches]).reshape(-1,1,2)
            ptsRight = np.float32([ kpRight[m.trainIdx].pt for m in matches]).reshape(-1,1,2)
            H, mask = cv2.findHomography(ptsLeft, ptsRight, cv2.RANSAC, 5.0)
            self.homography = H

            matchesMask = mask.ravel().tolist()
            h,w = left.shape
            pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
            dst = cv2.perspectiveTransform(pts,H)
            img2 = cv2.polylines(right,[np.int32(dst)],True,255,3, cv2.LINE_AA)
            draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                            singlePointColor = None,
                            matchesMask = matchesMask, # draw only inliers
                            flags = 2)
            img3 = cv2.drawMatches(left,kpLeft,right,kpRight,matches,None,**draw_params)
        else:
            print('Cannot find enough keypoints!')

    def publish_joint_color(self, timer):
        try:
            if self.rgb_image_left is None or self.rgb_image_right is None:
                return
            if self.homography is None:
                self.calculate_homography()
                return
            left = self.rgb_image_left
            right = self.rgb_image_right
            # stitch images using precomputed homography
            H = np.linalg.inv(self.homography)
            result = cv2.warpPerspective(right, H,
                                (left.shape[1] + right.shape[1], left.shape[0]))
            result[0:right.shape[0], 0:left.shape[1]] = left
            data = self.bridge.cv2_to_imgmsg(result, "bgr8")
            self.joint_color_pub.publish(data)
        except Exception as e:
            print(traceback.print_exc())

    def publish_joint_depth(self, timer):
        if self.depth_image_left is None or self.depth_image_right is None:
            return
        if self.homography is None:
            # Wait for RGB homograph to be calculated
            return
        left = self.depth_image_left
        right = self.depth_image_right
        # stitch images using precomputed homography
        H = np.linalg.inv(self.homography)
        result = cv2.warpPerspective(right, H,
                            (left.shape[1] + right.shape[1], left.shape[0]))
        result[0:right.shape[0], 0:left.shape[1]] = left
        data = self.bridge.cv2_to_imgmsg(result, "mono16")
        self.joint_depth_pub.publish(data)
        

    def display_images(self, timer):
        if self.rgb_image_left is None or self.rgb_image_right is None:
            return
        if self.homography is None:
            self.calculate_homography()
            return
        try:
            #self.publish_joint_color()
            # Show images
            cv2.waitKey(3)

        except Exception as e:
            rospy.logwarn(e)         

if __name__ == '__main__':
    try:
        rospy.init_node('ImageStitch', anonymous=True)
        processor = ImageStitch()
        rospy.spin()
    except rospy.ROSInterruptException:
        print('Image processing node failed!')
        pass
    cv2.destroyAllWindows()