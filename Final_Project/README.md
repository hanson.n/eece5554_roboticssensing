# Cyclessistant

### An augmented situational tool for urban cyclists

#### Built using OpenCV + YOLOv4 + DeepSORT + ROS

#### How to run

This repository contains other repositories as submodules -- use `git clone --recursive`.

- Copy bag file containing image camera data from [here](https://drive.google.com/file/d/1x72y97xae0TuG0Cb0UXHFQGORWrReO17/view?usp=sharing)
- `catkin_make` the repository and `source devel/setup.bash`
- Play the downloaded bagfile
- `roslaunch launch/post_process.launch`