# Lab 2
## IMU and Magnetometer Characterization

To run, `catkin_make` workspace under the `LAB2` directory

Source `devel/setup.bash`

`cd launch && roslaunch run_driver.launch`

OR

`rosrun imu_driver driver.py _device:=<<FILENAME>>`