#!/usr/bin/env python

#-*-coding: utf-8-*-

'''
Author: Nathaniel Hanson
Date: 02/21/2020
File: driver.py

Purpose: Process Raw IMU data from sensor or emulator
'''

import rospy
import serial
import sys
from math import radians
import tf
import tf2_ros
from std_msgs.msg import String, Header
from sensor_msgs.msg import Imu, MagneticField

pub_imu = rospy.Publisher('/imu_data', Imu, queue_size=1000)
pub_mag = rospy.Publisher('/mag_data', MagneticField, queue_size=1000)
pub_raw = rospy.Publisher('/raw_data', String, queue_size=1000)
seq = 0

def parse_line(line):
    '''
    Process raw IMU string data
    '''
    parts = line.split(',')
    if parts[0] == '$VNYMR':
        return parse_parts(parts)
    else:
        return {}

def parse_parts(parts):
    '''
    Take list of IMU VNYMR data attributes and convert strings to correct data type
    '''
    try:
        return {
            'yaw': float(parts[1]),
            'pitch': float(parts[2]),
            'roll': float(parts[3]),
            'magx': float(parts[4]),
            'magy': float(parts[5]),
            'magz': float(parts[6]),
            'accelx': float(parts[7]),
            'accely': float(parts[8]),
            'accelz': float(parts[9]),
            'gyrox': float(parts[10]),
            'gyroy': float(parts[11]),
            'gyroz': float(parts[12].split('*')[0])
        }
    except Exception as e:
        rospy.logerr('Error parsing IMU data {}'.format(e))
        rospy.logerr(parts)
        return {}

def construct_msgs(params):
    '''
    Turn dictionary of IMU parsed data into ROS IMU/Magnetometer messages
    '''
    global seq
    # Construct header
    shared_header = Header()
    shared_header.seq = seq
    shared_header.stamp = rospy.Time.now()
    shared_header.frame_id = '1'
    # Construct IMU message
    imu_out = construct_imu(params, shared_header)
    # Construct magnetometer message
    mag_out = construct_mag(params, shared_header)
    # Increment sequence number
    seq += 1
    return imu_out, mag_out

def construct_imu(parts, head):
    '''
    Construct IMU message from parsed ASCII string
    '''
    to_send = Imu()
    to_send.header = head
    quaternion = tf.transformations.quaternion_from_euler(
        radians(parts.get('roll')),
        radians(parts.get('pitch')),
        radians(parts.get('yaw'))
    )
    to_send.orientation.x = quaternion[0]
    to_send.orientation.y = quaternion[1]
    to_send.orientation.z = quaternion[2]
    to_send.orientation.w = quaternion[3]
    to_send.linear_acceleration.x = parts.get('accelx')
    to_send.linear_acceleration.y = parts.get('accely')
    to_send.linear_acceleration.z = parts.get('accelz')
    to_send.angular_velocity.x = parts.get('gyrox')
    to_send.angular_velocity.y = parts.get('gyroy')
    to_send.angular_velocity.z = parts.get('gyroz')
    to_send.orientation_covariance = [-1, 0, 0, 0, 0, 0, 0, 0, 0]
    to_send.angular_velocity_covariance = [-1, 0, 0, 0, 0, 0, 0, 0, 0]
    to_send.linear_acceleration_covariance = [-1, 0, 0, 0, 0, 0, 0, 0, 0]
    return to_send

def construct_mag(parts, head):
    '''
    Construct magnetometer message from parsed ASCII string
    '''
    to_send = MagneticField()
    to_send.header = head
    to_send.magnetic_field.x = parts.get('magx')
    to_send.magnetic_field.y = parts.get('magy')
    to_send.magnetic_field.z = parts.get('magz')
    to_send.magnetic_field_covariance = [-1, 0, 0, 0, 0, 0, 0, 0, 0]
    return to_send

def send_msg(to_send_imu, to_send_mag):
    '''
    Publish message on the IMU/Magnetometer topics
    '''
    pub_imu.publish(to_send_imu)
    pub_mag.publish(to_send_mag)

def imu_driver():
    '''
    Main execution for IMU driver
    '''
    rospy.init_node('imu_driver')
    print(rospy.get_param_names())
    try:
        port = rospy.get_param('/imu_driver/device')
    except:
        raise(ValueError, 'No param specified')
        sys.exit(1)
    try:
        serial_buad = rospy.get_param('/IMU_Parser/baud')
    except:
        serial_buad = 115200
    try:
        sampling_rate = rospy.get_param('/IMU_Parser/sampling_rate')
    except:
        sampling_rate = 1000
    # Initialize serial connection
    serial_port = serial.Serial(port, serial_buad, timeout=3)
    while not rospy.is_shutdown():
        line = serial_port.readline().strip()
        #rospy.loginfo(line)
        pub_raw.publish(line)
        if not line:
            rospy.logdebug('No IMU data received')
        else:
            data = parse_line(line)
            # If correct data
            if data:
                rospy.loginfo(data)
                imu_msg, mag_msg = construct_msgs(data)
                send_msg(imu_msg, mag_msg)
        # Sleep while in loop
        rospy.sleep(1/sampling_rate)
    # Close port before shutting down
    serial_port.close()


if __name__ == '__main__':
    try:
        imu_driver()
    except rospy.ROSInterruptException:
        pass