%% Magnetometer Calbiration

% Read in the bagfile
%clear all
circleBag = rosbag('C:/Users/nhans/Downloads/Data2/Data2/Circles.bag');

bSel = select(circleBag,'Topic','/MAG');
msgStructs = readMessages(bSel,'DataFormat','struct');

X = [];
Y = [];
Z = [];

for i=1:length(msgStructs)
    t = msgStructs(i);
    X = [X; t{1}.MagneticField.X];
    Y = [Y; t{1}.MagneticField.Y];
    Z = [Z; t{1}.MagneticField.Z];
end
%% 2b - 1 Correct Magnetometer Data

% Plot data
f2 = figure;
figure(f2);
hold on;
% Calculate coefficients for soft and hard iron corrections
D = [X(:) Y(:), Z(:)];
% Use builtin function to perform calibration
[soft,hard,expmfs] = magcal(D);
% Correct Data
hardCorrection = D-hard;
finalMag = (D-hard)*soft; % calibrated data
plot(X,Y);
plot(hardCorrection(:,1), hardCorrection(:,2));
plot(finalMag(:,1), finalMag(:,2));
legend({'Raw Data','Hard Iron Correct', 'Soft Iron Correction'},'Location','southwest');
xlabel('X Direction Field Strength (Gauss)');
ylabel('Y Direction Field Stength (Gauss)');
title('Corrective Measures on Magnetometer Calibration Data', 'Using Soft and Hard Iron Methods');
%% 2b - 2  Calculate Yaw Angles

%Calculate the yaw angle from the corrected magnetometer readings

% Read in data from car trip
tripBag = rosbag('C:/Users/nhans/Downloads/Data2/Data2/Driving.bag');

btripSel = select(tripBag,'Topic','/MAG');
msgStructsTrip = readMessages(btripSel,'DataFormat','struct');

X_Trip = [];
Y_Trip = [];
Z_Trip = [];
time_stamps = [];

for i=1:length(msgStructsTrip)
    t = msgStructsTrip(i);
    X_Trip = [X_Trip; t{1}.MagneticField.X];
    Y_Trip = [Y_Trip; t{1}.MagneticField.Y];
    Z_Trip = [Z_Trip; t{1}.MagneticField.Z];
    % Convert time to a usable format
    tempTime = double(t{1}.Header.Stamp.Sec )+ 1e-9 * double(t{1}.Header.Stamp.Nsec);
    time_stamps = [time_stamps; double(tempTime)];
end

% Correct the data using hard/soft iron corrections
D = [X_Trip(:) Y_Trip(:) Z_Trip(:)];
finalMagTrip = (D-hard)*soft;
yaw_mag = atan2(finalMagTrip(:,2), finalMagTrip(:,1));
mag_time_steps = linspace(0, time_stamps(end) - time_stamps(1), length(time_stamps))'; 
mag(:,4) = mag_time_steps;
%%
% Read in IMU data
close all;
imuSel = select(tripBag,'Topic','/IMU');
imuTrip = readMessages(imuSel,'DataFormat','struct');
gyro = [];
orientation = [];
for i=1:length(imuTrip)
    t = imuTrip(i);
    % Convert time to a usable format
    tempTime = double(t{1}.Header.Stamp.Sec )+ (1e-9 * double(t{1}.Header.Stamp.Nsec));
    tempRow = [double(t{1}.AngularVelocity.X) double(t{1}.AngularVelocity.Y) double(t{1}.AngularVelocity.Z) double(tempTime)];
    gyro = [gyro; tempRow];
    tempOr = [quat2eul([double(t{1}.Orientation.W) double(t{1}.Orientation.X) double(t{1}.Orientation.Y) double(t{1}.Orientation.Z)]) double(tempTime)];
    orientation = [orientation; tempOr];
end
imu_time_steps = linspace(0, gyro(end,4) - gyro(1,4), length(gyro)); 
gyro(:,5) = imu_time_steps;
%% Plot IMU and Mag Data 
close all
% Grab the raw quaternion data
% Plot trip data
f3 = figure;
figure(f3);
yaw_imu = -deg2rad(cumtrapz(gyro(:,3)));
yaw_imu = unwrap(yaw_imu);
yaw_mag = unwrap(yaw_mag);
% Data jumps by PI randomly in the middle of the dataset, correct this
% deivation
yaw_mag(5889:end,:) = yaw_mag(5889:end) + 3.14159;
yaw_imu = filloutliers(yaw_imu,'previous','movmean',5);
%yaw_imu = smooth(yaw_imu, 0.001, 'rloess');
yaw_mag = filloutliers(yaw_mag,'previous','movmean',5);
%yaw_mag = smooth(yaw_mag, 0.001, 'rloess');
% Account for bias
yaw_mag = yaw_mag + (yaw_imu(1) - yaw_mag(1));
hold on
plot(imu_time_steps, yaw_imu)
plot(mag_time_steps, yaw_mag);

%% Plot raw yaw
yaw_raw = unwrap(orientation(:,1));
plot(imu_time_steps, -deg2rad(yaw_raw));
%% Apply High Pass and Low Pass Filters to create the complimentary filter
yaw_low_pass = lowpass(yaw_mag, 0.000001, 'Steepness',0.99);
yaw_high_pass = highpass(yaw_imu, 0.5, 'Steepness',0.99);
minLength = min(length(yaw_low_pass), length(yaw_high_pass));
yaw_low_pass = yaw_low_pass(1:minLength);
yaw_high_pass = yaw_high_pass(1:minLength);
alpha = 0.50;
yaw_combined = (1-alpha) *  yaw_high_pass + alpha * yaw_low_pass;
plot(mag_time_steps, yaw_combined);
%% Format the graph
title('Calculated Yaw Measurements over Time');
xlabel('Time Elapsed (seconds)');
ylabel('Yaw Angle (radians)');
legend('IMU Integration', 'Magnetometer Readings', 'Raw Yaw Orientation', 'Complimentary Filter', 'Location','southwest');

%% Forward Velocity Estimation
close all;
% Integrate linear acceleration
linearAccel = [];
for i=1:length(imuTrip)
    t = imuTrip(i);
    % Convert time to a usable format
    tempTime = double(t{1}.Header.Stamp.Sec )+ 1e-9 * double(t{1}.Header.Stamp.Nsec);
    % Account for biases identified in part 1
    BIAS_X = 0.02049;
    BIAS_Y = 0.01273;
    BIAS_Z = -9.49664;
    tempRow = [double(t{1}.LinearAcceleration.X) double(t{1}.LinearAcceleration.Y) double(t{1}.LinearAcceleration.Z) double(tempTime)];
    tempRow = [tempRow norm(tempRow(1:3))];
    linearAccel = [linearAccel; tempRow];
end
linearAccel(:,6) = imu_time_steps;
% Account for overall acceleration bias
% Calculate account for bias
finalVel = [];
% Perform an integration when the vehicle is in motion
store = [];
i = 1;
STOP_WATCH = 0;
forwardAccel = max(linearAccel(:,1) + 0.90, 0);
while i < length(linearAccel)
    % Lookahead and determine how much we are varying, try to eliminate
    % segments where we are stopped, then we can restart the integration
    THRESH = 0.34;
   if forwardAccel(i) > THRESH
       % Keep track of this value, it might be useful
       tRow = [forwardAccel(i) linearAccel(i,6)];
       store = [store; tRow];
       STOP_WATCH = 0;
   elseif STOP_WATCH > 250
       if size(store, 1) > 1
         finalVel = [finalVel; cumtrapz(store(:,2), store(:,1)); 0];
       else
         finalVel= [finalVel; zeros(size(store, 1),1); 0];
       end
       store = [];
       STOP_WATCH = 0;
   else
       STOP_WATCH = STOP_WATCH + 1;
       store = [store; 0 linearAccel(i,6)];
   end
   i = i + 1;
end
if size(store, 1) > 1
   finalVel = [finalVel; cumtrapz(store(:,2), store(:,1)); 0];
else
   finalVel= [finalVel; zeros(size(store, 1),1); 0];
end
plot(linearAccel(:,6), linearAccel(:,1));
hold on
plot(linearAccel(:,6), finalVel);
%% Calculate from GPS data
hold on
btripGPS = select(tripBag,'Topic','/GPS');
msgStructsGPS = readMessages(btripGPS,'DataFormat','struct');
gps = [];
gps_lat_lon = [];
for i=1:length(msgStructsGPS)
    t = msgStructsGPS(i);
    % Convert time to a usable format
    tempTime = double(t{1}.Header.Stamp.Sec )+ 1e-9 * double(t{1}.Header.Stamp.Nsec);
    % Calculate speed
    if i > 1
        dist = sqrt((gps(end,1) - double(t{1}.UtmEasting))^2 + (gps(end,2) - double(t{1}.UtmNorthing))^2);
        speed = dist / (tempTime - gps(end,4));
    else
        speed = 0;
    end
    tempRow = [double(t{1}.UtmEasting) double(t{1}.UtmNorthing) speed double(tempTime)];
    gps = [gps; tempRow];
    gps_lat_lon = [gps_lat_lon; double(t{1}.Lattitude) double(t{1}.Longitude)];
end
plot(linspace(0, gps(end,4) - gps(1,4), length(gps)),gps(:,3));
title('Calculated Velocity over Time');
xlabel('Time Elapsed (seconds)');
ylabel('Forward Velocity (m/s), Acceleration (m/s^2)');
legend('Linear Acceleration','Integrated Velocity','GPS Derived Velocity');
% Find regions where the GPS velocity is zero or close to it
gps(:,5) = gps(:,4)-min(gps(:,4));
ts = gps(find(gps(:,3) < 0.5),:);
ts = ts(:,5);

%% Plot raw GPS values
plot(gps_lat_lon(:,2), gps_lat_lon(:,1), 'LineWidth',5);
plot_openstreetmap('Alpha', 0.4, 'Scale', 2);
title('GPS Route', 'FontSize', 17);
xlabel('Longitude (degrees)');
ylabel('Latitude (degrees)');
%% Dead Reckoning with IMU Data
close all
% Part 1
% Compute w*xdot and ydouble dot
w = gyro(:,3);
xdot = finalVel;
ydoubledot = linearAccel(:,2); %- mean(linearAccel(:,2));
f2 = figure;
figure(f2);
plot(imu_time_steps, ydoubledot);
hold on
plot(imu_time_steps, w.*xdot);
xlabel('Time Elapsed (s)');
ylabel('Acceleration (m/s^2)');
title('Comparison of Measured vs. Calculated Linear Acceleration');
legend({'$\ddot{y_{obs}}$', '$\omega\dot{X}$'}, 'Interpreter', 'latex')

f3 = figure;
figure(f3);
x_e = cumtrapz(imu_time_steps, xdot.*cos(deg2rad(yaw_raw)));
x_n = cumtrapz(imu_time_steps, xdot.*sin(deg2rad(yaw_raw)));
x_e = x_e - mean(x_e);
x_n = x_n - mean(x_n);
% Apply a homogenous rotation about the "z-axis" to correct the orientation
tempPoints = [-x_e/2.25 x_n/2.25];
theta = pi/2;
rot = [cos(theta) -sin(theta); sin(theta) cos(theta)];
for i=1:size(tempPoints,1)
     tempPoints(i,:) = (rot * tempPoints(i,:)')';
end

plot(tempPoints(:,1), tempPoints(:,2));
hold on
% Correct gps data
gps_east = gps(:,1) - mean(gps(:,1));
gps_north = gps(:,2) - mean(gps(:,2));
plot(gps_east, gps_north);
xlabel('Distance - East (m)');
ylabel('Distance - North (m)');
title('Dead Reckoning vs. GPS Measurements');
legend('Dead Reckoning Integration', 'GPS Pings');

%% Alan Deviation Measurements
close all
% Using sample data set
% Load logged data from one axis of a three-axis gyroscope. This recording
% was done over a six hour period with a 100 Hz sampling rate.
load('LoggedSingleAxisGyroscope', 'omega', 'Fs')
t0 = 1/Fs;
theta = cumsum(omega, 1)*t0;
maxNumM = 100;
L = size(theta, 1);
maxM = 2.^floor(log2(L/2));
m = logspace(log10(1), log10(maxM), maxNumM).';
m = ceil(m); % m must be an integer.
m = unique(m); % Remove duplicates.

tau = m*t0;

avar = zeros(numel(m), 1);
for i = 1:numel(m)
    mi = m(i);
    avar(i,:) = sum( ...
        (theta(1+2*mi:L) - 2*theta(1+mi:L-mi) + theta(1:L-2*mi)).^2, 1);
end
avar = avar ./ (2*tau.^2 .* (L - 2*m));

adev = sqrt(avar);

figure
loglog(tau, adev)
title('Allan Deviation')
xlabel('\tau');
ylabel('\sigma(\tau)')
grid on
axis equal

[avarFromFunc, tauFromFunc] = allanvar(omega, m, Fs);
adevFromFunc = sqrt(avarFromFunc);

figure
loglog(tau, adev, tauFromFunc, adevFromFunc);
title('Allan Deviations')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('Manual Calculation', 'allanvar Function')
grid on
axis equal

% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = -0.5;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the angle random walk coefficient from the line.
logN = slope*log(1) + b;
N = 10^logN

% Plot the results.
tauN = 1;
lineN = N ./ sqrt(tau);
figure
loglog(tau, adev, tau, lineN, '--', tauN, N, 'o')
title('Allan Deviation with Angle Random Walk')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_N')
text(tauN, N, 'N')
grid on
axis equal

% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = 0.5;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the rate random walk coefficient from the line.
logK = slope*log10(3) + b;
K = 10^logK

% Plot the results.
tauK = 3;
lineK = K .* sqrt(tau/3);
figure
loglog(tau, adev, tau, lineK, '--', tauK, K, 'o')
title('Allan Deviation with Rate Random Walk')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_K')
text(tauK, K, 'K')
grid on
axis equal

% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = 0;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the bias instability coefficient from the line.
scfB = sqrt(2*log(2)/pi);
logB = b - log10(scfB);
B = 10^logB

% Plot the results.
tauB = tau(i);
lineB = B * scfB * ones(size(tau));
figure
loglog(tau, adev, tau, lineB, '--', tauB, scfB*B, 'o')
title('Allan Deviation with Bias Instability')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_B')
text(tauB, scfB*B, '0.664B')
grid on
axis equal

tauParams = [tauN, tauK, tauB];
params = [N, K, scfB*B];
figure
loglog(tau, adev, tau, [lineN, lineK, lineB], '--', ...
    tauParams, params, 'o')
title('Allan Deviation with Noise Parameters')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_N', '\sigma_K', '\sigma_B')
text(tauParams, params, {'N', 'K', '0.664B'})
grid on
axis equal
%% Allan Variation on long calibration data

% Read in the bagfile
clear all
calBag = rosbag('C:/Users/nhans/Downloads/stationarylong.bag');

bAlan = select(calBag,'Topic','/imu_data');
msgAlanGyro = readMessages(bAlan,'DataFormat','struct');

gyroAllan = [];
orientationAllan = [];
for i=1:length(msgAlanGyro)
    t = msgAlanGyro(i);
    % Convert time to a usable format
    tempTime = double(t{1}.Header.Stamp.Sec )+ (1e-9 * double(t{1}.Header.Stamp.Nsec));
    tempRow = [double(t{1}.AngularVelocity.X) double(t{1}.AngularVelocity.Y) double(t{1}.AngularVelocity.Z) double(tempTime)];
    gyroAllan = [gyroAllan; tempRow];
    tempOr = [quat2eul([double(t{1}.Orientation.W) double(t{1}.Orientation.X) double(t{1}.Orientation.Y) double(t{1}.Orientation.Z)]) double(tempTime)];
    orientationAllan = [orientationAllan; tempOr];
end
%% Allan Deviation with loaded data set
% Analyze the y axis of the gyroscope
Fs = 40;
t0 = 1/Fs;
theta = cumsum(orientationAllan(:,2), 1)*t0;
maxNumM = 100;
L = size(theta, 1);
maxM = 2.^floor(log2(L/2));
m = logspace(log10(1), log10(maxM), maxNumM).';
m = ceil(m); % m must be an integer.
m = unique(m); % Remove duplicates.

tau = m*t0;

avar = zeros(numel(m), 1);
for i = 1:numel(m)
    mi = m(i);
    avar(i,:) = sum( ...
        (theta(1+2*mi:L) - 2*theta(1+mi:L-mi) + theta(1:L-2*mi)).^2, 1);
end
avar = avar ./ (2*tau.^2 .* (L - 2*m));

adev = sqrt(avar);

% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = -0.5;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the angle random walk coefficient from the line.
logN = slope*log(1) + b;
N = 10^logN;

% Plot the results.
tauN = 1;
lineN = N ./ sqrt(tau);

% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = 0.5;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the rate random walk coefficient from the line.
logK = slope*log10(3) + b;
K = 10^logK;

% Plot the results.
tauK = 3;
lineK = K .* sqrt(tau/3);


% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = 0;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the bias instability coefficient from the line.
scfB = sqrt(2*log(2)/pi);
logB = b - log10(scfB);
B = 10^logB;

% Plot the results.
tauB = tau(i);
lineB = B * scfB * ones(size(tau));

tauParams = [tauN, tauK, tauB];
params = [N, K, scfB*B];
figure
loglog(tau, adev, tau, [lineN, lineK, lineB], '--', ...
    tauParams, params, 'o')
title('Allan Deviation with Noise Parameters - Angular Y')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_N', '\sigma_K', '\sigma_B')
text(tauParams, params, {'N', 'K', '0.664B'})
grid on
axis equal
