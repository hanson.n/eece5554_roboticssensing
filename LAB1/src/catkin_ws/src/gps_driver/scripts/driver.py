#!/usr/bin/env python

#-*-coding: utf-8-*-

'''
Author: Nathaniel Hanson
Date: 02/16/2020
File: driver.py

Purpose: Process Raw GPS data from sensor or emulator
'''

import rospy
import serial
import utm
from math import sin,pi
from datetime import datetime
from gps_driver.msg import GPS
from std_msgs.msg import String, Header

pub = rospy.Publisher('/gps_data', GPS, queue_size=10)
pub_raw = rospy.Publisher('/raw_data', String, queue_size=10)
seq = 0

def parse_line(line):
    '''
    Process raw GPS string data
    '''
    parts = line.split(',')
    if parts[0] == '$GPGGA':
        return gpgga(parts)
    else:
        return {}

def gpgga(parts):
    '''
    Take list of GPGGA gps data and convert strings to correct data type
    '''
    try:
        lat, lon = correct_lat_lon(*parts[2:6])
        easting, northing, zone, letter = utm.from_latlon(lat,lon)
        time = parts[1]
        time_parts = {
            'hours': int(time[0:2]),
            'minutes': int(time[2:4]),
            'seconds': int(time[4:6]),
            'microseconds': int(float(time[6:]) * 1e6)
        }
        altitude = float(parts[9])
        rospy.loginfo(parts)
        return {
            'lat':lat,
            'lon':lon,
            'altitude':altitude,
            'easting':easting,
            'northing':northing,
            'zone':zone,
            'letter':letter,
            'time': time_parts,
            'num_sats':int(parts[7]),
            'hdop': float(parts[8])
        }
    except Exception as e:
        rospy.logerr('Error parsing GPGGA data {}'.format(e))
        rospy.logerr(parts)
        return {}

def correct_lat_lon(lat,lat_dir,lon,lon_dir):
    '''
    Convert Lat/Lon string to correct decimal value
    '''
    lat, lon  = dms_to_dec(lat, 'lat'), dms_to_dec(lon, 'lon')
    if lat_dir == 'N':
        lat *= 1
    elif lat_dir == 'S':
        lat *= -1
    if lon_dir == 'W':
        lon *= -1
    elif lon_dir == 'E':
        lat *= 1
    return lat, lon

def dms_to_dec(coord, ctype):
    '''
    Convert lat/lon in degrees-minutes-seconds to decimal format
    '''
    if ctype == 'lat':    
        degrees = float(coord[0:2])
        minutes = float(coord[2:])
    elif ctype == 'lon':
        degrees = float(coord[0:3])
        minutes = float(coord[3:])
    return degrees + minutes/60

def construct_msg(params):
    '''
    Turn dictionary of GPS parsed data into ROS GPS message
    '''
    global seq
    # Get current date
    time = datetime.utcnow()
    time.replace(
        hour=params.get('time').get('hours'), 
        minute=params.get('time').get('minutes'), 
        second=params.get('time').get('seconds'),
        microsecond=params.get('time').get('microseconds')
        )
    # Workarounds since datetime doesn't have a good epoch conversion in python2.7
    epoch = (time - datetime(1970,1,1)).total_seconds()
    to_send = GPS()
    # Construct header
    to_send.header = Header()
    to_send.header.seq = seq
    to_send.header.stamp = rospy.Time.from_sec(epoch)
    to_send.header.frame_id = '1'
    to_send.lat = params.get('lat')
    to_send.lon = params.get('lon')
    to_send.altitude = params.get('altitude')
    to_send.utm_easting = params.get('easting')
    to_send.utm_northing = params.get('northing')
    to_send.zone = params.get('zone')
    to_send.letter = params.get('letter')
    to_send.num_sats = params.get('num_sats')
    to_send.hdop = params.get('hdop')
    rospy.loginfo(to_send)
    # Increment sequence number
    seq += 1
    return to_send

def send_msg(to_send):
    '''
    Publish message on the GPS topic
    '''
    pub.publish(to_send)

def gps_driver():
    '''
    Main execution for GPS driver
    '''
    rospy.init_node('gps_driver')
    print(rospy.get_param_names())
    port = rospy.get_param('/GPS_Parser/port')
    serial_buad = rospy.get_param('/GPS_Parser/baud')
    sampling_rate = rospy.get_param('/GPS_Parser/sampling_rate')
    # Initialize serial connection
    serial_port = serial.Serial(port, serial_buad, timeout=3)
    while not rospy.is_shutdown():
        line = serial_port.readline().strip()
        rospy.loginfo(line)
        pub_raw.publish(line)
        if not line:
            rospy.logdebug('No GPS data received')
        else:
            data = parse_line(line)
            # If correct data
            if data:
                rospy.loginfo(data)
                to_send = construct_msg(data)
                send_msg(to_send)
        # Sleep while in loop
        rospy.sleep(1/sampling_rate)
    # Close port before shutting down
    serial_port.close()


if __name__ == '__main__':
    try:
        gps_driver()
    except rospy.ROSInterruptException:
        pass