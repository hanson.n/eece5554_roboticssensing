# Repository for Lab 1, GPS Error Analysis

To run driver build catkin workspace, source setup.bash, and run `roslaunch run_driver.launch`

N.B. The launch file will need to be modified to contain the correct serial port and baud rate

Bag files containing the GPS data are located under the gps_driver package directory
