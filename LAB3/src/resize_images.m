cd(fileparts(which('resize_images.m')))
imgBasePath = '../img/camera_calibration/';
filepath = fileparts(which('resize_images.m'));
srcFiles = dir([imgBasePath '/*.JPG']);  % the folder in which ur images exists
for i = 1 : length(srcFiles)
    filename = strcat(imgBasePath,srcFiles(i).name);
    im = imread(filename);
    k=imresize(im,[size(im,1)/4,size(im,2)/4]);
    imwrite(k, filename, 'jpg');
end