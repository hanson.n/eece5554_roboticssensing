% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 826.357719113129406 ; 824.448658000238879 ];

%-- Principal point:
cc = [ 505.025091903157204 ; 372.651980969468752 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.121098327588336 ; -0.448860533799791 ; 0.000865914088013 ; 0.000418126309766 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 4.182012057510141 ; 4.153476612713246 ];

%-- Principal point uncertainty:
cc_error = [ 4.853583916542531 ; 4.176981183061461 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.018275434253347 ; 0.084065424399423 ; 0.002135947830529 ; 0.002427081463879 ; 0.000000000000000 ];

%-- Image size:
nx = 1008;
ny = 756;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 18;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -2.213360e+00 ; -2.170225e+00 ; 4.396014e-02 ];
Tc_1  = [ -1.064295e+02 ; -5.624887e+01 ; 3.108446e+02 ];
omc_error_1 = [ 5.264032e-03 ; 5.170926e-03 ; 1.128665e-02 ];
Tc_error_1  = [ 1.837983e+00 ; 1.607182e+00 ; 1.774092e+00 ];

%-- Image #2:
omc_2 = [ -1.996261e+00 ; -2.017945e+00 ; 4.417842e-01 ];
Tc_2  = [ -1.464000e+02 ; -5.758845e+01 ; 4.019996e+02 ];
omc_error_2 = [ 5.739357e-03 ; 4.746032e-03 ; 9.429581e-03 ];
Tc_error_2  = [ 2.384682e+00 ; 2.101481e+00 ; 2.093807e+00 ];

%-- Image #3:
omc_3 = [ -1.997379e+00 ; -1.910563e+00 ; -3.535930e-01 ];
Tc_3  = [ -1.511698e+02 ; 2.617892e+01 ; 3.530028e+02 ];
omc_error_3 = [ 4.998926e-03 ; 6.191458e-03 ; 9.613937e-03 ];
Tc_error_3  = [ 2.072639e+00 ; 1.879741e+00 ; 2.188408e+00 ];

%-- Image #4:
omc_4 = [ 2.077397e+00 ; 2.089833e+00 ; -4.105759e-01 ];
Tc_4  = [ -1.057216e+02 ; -9.234331e+01 ; 4.515386e+02 ];
omc_error_4 = [ 4.842179e-03 ; 6.294897e-03 ; 1.137625e-02 ];
Tc_error_4  = [ 2.668519e+00 ; 2.307162e+00 ; 2.336404e+00 ];

%-- Image #5:
omc_5 = [ 1.976975e+00 ; 1.968501e+00 ; -1.030933e+00 ];
Tc_5  = [ -1.439578e+02 ; -6.555549e+01 ; 6.121371e+02 ];
omc_error_5 = [ 4.187276e-03 ; 6.653034e-03 ; 9.627291e-03 ];
Tc_error_5  = [ 3.623884e+00 ; 3.154689e+00 ; 2.987309e+00 ];

%-- Image #6:
omc_6 = [ -2.151754e+00 ; -1.585449e+00 ; 1.558794e-02 ];
Tc_6  = [ -1.434903e+02 ; 8.681483e+00 ; 4.342888e+02 ];
omc_error_6 = [ 5.427626e-03 ; 4.936005e-03 ; 9.063307e-03 ];
Tc_error_6  = [ 2.555823e+00 ; 2.246990e+00 ; 2.347656e+00 ];

%-- Image #7:
omc_7 = [ 2.032483e+00 ; 1.945017e+00 ; 4.624784e-01 ];
Tc_7  = [ -1.169685e+02 ; -4.835788e+01 ; 4.218819e+02 ];
omc_error_7 = [ 6.407842e-03 ; 5.771993e-03 ; 1.153473e-02 ];
Tc_error_7  = [ 2.522127e+00 ; 2.180732e+00 ; 2.602612e+00 ];

%-- Image #8:
omc_8 = [ -1.910434e+00 ; -1.908881e+00 ; -5.031209e-01 ];
Tc_8  = [ -1.024533e+02 ; -3.407950e+01 ; 3.964989e+02 ];
omc_error_8 = [ 4.372207e-03 ; 5.921815e-03 ; 9.030876e-03 ];
Tc_error_8  = [ 2.331572e+00 ; 2.038010e+00 ; 2.324360e+00 ];

%-- Image #9:
omc_9 = [ 2.229341e+00 ; 1.845359e+00 ; -1.903677e-01 ];
Tc_9  = [ -9.039536e+01 ; -9.320346e+01 ; 3.906639e+02 ];
omc_error_9 = [ 5.416170e-03 ; 5.630149e-03 ; 1.095723e-02 ];
Tc_error_9  = [ 2.323999e+00 ; 1.990143e+00 ; 2.138731e+00 ];

%-- Image #10:
omc_10 = [ 2.640357e+00 ; 1.441359e+00 ; 6.244156e-01 ];
Tc_10  = [ -1.878868e+01 ; 4.742466e+00 ; 3.477441e+02 ];
omc_error_10 = [ 7.887122e-03 ; 2.897914e-03 ; 1.123757e-02 ];
Tc_error_10  = [ 2.040059e+00 ; 1.741252e+00 ; 2.060664e+00 ];

%-- Image #11:
omc_11 = [ -2.200945e+00 ; -2.169123e+00 ; 9.804264e-02 ];
Tc_11  = [ -7.070008e+01 ; -3.057033e+01 ; 6.947049e+02 ];
omc_error_11 = [ 1.632261e-02 ; 1.809605e-02 ; 3.807611e-02 ];
Tc_error_11  = [ 4.079599e+00 ; 3.527298e+00 ; 4.562153e+00 ];

%-- Image #12:
omc_12 = [ -1.889642e+00 ; -1.878717e+00 ; -4.942927e-01 ];
Tc_12  = [ -1.003492e+02 ; -4.425596e+01 ; 2.718487e+02 ];
omc_error_12 = [ 3.828119e-03 ; 5.340238e-03 ; 7.973511e-03 ];
Tc_error_12  = [ 1.607908e+00 ; 1.412512e+00 ; 1.617846e+00 ];

%-- Image #13:
omc_13 = [ 1.938137e+00 ; 1.983422e+00 ; -5.242414e-01 ];
Tc_13  = [ -1.096605e+02 ; -7.005812e+01 ; 3.243711e+02 ];
omc_error_13 = [ 3.449878e-03 ; 5.339585e-03 ; 8.640258e-03 ];
Tc_error_13  = [ 1.915819e+00 ; 1.676132e+00 ; 1.661570e+00 ];

%-- Image #14:
omc_14 = [ -1.752228e+00 ; -1.694955e+00 ; 8.324542e-01 ];
Tc_14  = [ -9.441360e+01 ; -5.467138e+01 ; 4.097768e+02 ];
omc_error_14 = [ 5.579530e-03 ; 4.273813e-03 ; 7.000963e-03 ];
Tc_error_14  = [ 2.412137e+00 ; 2.095490e+00 ; 1.729334e+00 ];

%-- Image #15:
omc_15 = [ 1.981689e+00 ; 1.883722e+00 ; 6.803966e-01 ];
Tc_15  = [ -4.442757e+01 ; -6.224121e+01 ; 2.528342e+02 ];
omc_error_15 = [ 5.719233e-03 ; 4.089744e-03 ; 8.180286e-03 ];
Tc_error_15  = [ 1.506134e+00 ; 1.280920e+00 ; 1.528650e+00 ];

%-- Image #16:
omc_16 = [ -2.347494e+00 ; -1.024687e+00 ; 1.122842e+00 ];
Tc_16  = [ -8.747910e+01 ; -2.364751e+01 ; 4.663988e+02 ];
omc_error_16 = [ 6.489973e-03 ; 3.322995e-03 ; 7.792551e-03 ];
Tc_error_16  = [ 2.751742e+00 ; 2.367201e+00 ; 1.971769e+00 ];

%-- Image #17:
omc_17 = [ 2.494349e+00 ; 1.310088e+00 ; -9.702266e-01 ];
Tc_17  = [ -1.238590e+02 ; 1.822630e+01 ; 4.859120e+02 ];
omc_error_17 = [ 4.896581e-03 ; 5.142615e-03 ; 9.514128e-03 ];
Tc_error_17  = [ 2.858399e+00 ; 2.483515e+00 ; 2.338762e+00 ];

%-- Image #18:
omc_18 = [ -2.122277e+00 ; -2.070764e+00 ; 3.179986e-01 ];
Tc_18  = [ -8.895449e+01 ; -9.011324e+01 ; 5.063267e+02 ];
omc_error_18 = [ 7.251795e-03 ; 6.240012e-03 ; 1.338503e-02 ];
Tc_error_18  = [ 2.977291e+00 ; 2.571235e+00 ; 2.663901e+00 ];

