# PA 1: _Learning ROS_

### :heavy_check_mark: Assignment completed on 01/30/2021


### :robot: ROS Basics 
- [X] Navigating the ROS Filesystem 
This tutorial introduces ROS filesystem concepts, and covers using the roscd, rosls, and rospack commandline tools. 

- [X] Creating a ROS Package 
This tutorial covers using roscreate-pkg or catkin to create a new package, and rospack to list package dependencies.

- [X] Building a ROS Package 
This tutorial covers the toolchain to build a package. 

- [X] Understanding ROS Nodes 
This tutorial introduces ROS graph concepts and discusses the use of roscore, rosnode, and rosrun commandline tools.

- [X] Understanding ROS Topics 
This tutorial introduces ROS topics as well as using the rostopic and rqt_plot commandline tools.

- [X] Understanding ROS Services and Parameters 
This tutorial introduces ROS services, and parameters as well as using the rosservice and rosparam commandline tools. 

- [X] Using rqt_console and roslaunch
This tutorial introduces ROS using rqt_console and rqt_logger_level for debugging and roslaunch for starting many nodes at once. If you use ROS fuerte or ealier distros where rqt isn't fully available, please see this page with this page that uses old rx based tools.

- [X] Using rosed to edit files in ROS
This tutorial shows how to use rosed to make editing easier. 

- [X] Creating a ROS msg and srv
This tutorial covers how to create and build msg and srv files as well as the rosmsg, rossrv and roscp commandline tools. 

### :computer: ROS Pub/Sub Nodes
- [X] Writing a Simple Publisher and Subscriber (Python)
This tutorial covers how to write a publisher and subscriber node in python. 

- [X] Writing a Simple Publisher and Subscriber (C++)
This tutorial covers how to write a publisher and subscriber node in C++. 

- [X] Examining the Simple Publisher and Subscriber
This tutorial examines running the simple publisher and subscriber. 

- [X] Writing a Simple Service and Client (Python)
This tutorial covers how to write a service and client node in python. 

- [X] Writing a Simple Service and Client (C++)
This tutorial covers how to write a service and client node in C++. 

- [X] Examining the Simple Service and Client
This tutorial examines running the simple service and client. 

### :bug: Debugging

- [X] Recording and playing back data
This tutorial will teach you how to record data from a running ROS system into a .bag file, and then to play back the data to produce similar behavior in a running system. 

- [X] Getting started with roswtf
Basic introduction to the roswtf tool. 

### :arrows_counterclockwise: Learning TF

- [X] Writing a tf2 broadcaster (Python)
This tutorial teaches you how to broadcast the state of a robot to tf.

- [X] Writing a tf2 listener (Python)
This tutorial teaches you how to use tf to get access to frame transformations. 

- [X] Adding a frame (Python)
This tutorial teaches you how to add an extra fixed frame to tf. 

- [X] Learning about tf2 and time (Python)
This tutorial teaches you to use the waitForTransform function to wait for a transform to be available on the tf tree. 

- [X] Time travel with tf2 (Python)
This tutorial teaches you about advanced time travel features of tf 