#! /bin/bash
rospack find rospy
roscd rospy
pwd
echo $ROS_PACKAGE_PATH
# Moving into subdirectories
roscd roscpp/cmake
pwd
# Log directory
roscd log # fail becuase we haven't run ROS before
# rosls
rosls roscpp_tutorials
# Some ROS tools support tab completion
rosls turtlesim
## Creating a catkin workspace
mkdir -p catkin_ws/src
catkin_make -DPYTHON_EXECUTABLE=/usr/bin/python3
source devel/setup.bash
# create catkin package
catkin_create_pkg beginner_tutorials std_msgs rospy roscpp
cd .. && catkin_make
source devel/setup.bash
# List first order dependencies
rospack depends1 beginner_tutorials
roscd beginner_tutorials
cat package.xml
# View indirect dependencies
rospack depends1 rospy
rospack depends beginner_tutorials
## Building a ROS package
source /opt/ros/melodic/setup.bash
# In workspace
catkin_make
# OR
catkin_make_install
# OR building from another place
catkin_make --source my_src_dir
# what should we have in our workspace at this point
ls 
# build devel src
## ROS Nodes
# start roscore
roscore
# in separate terminal
rosnode list
rosnode info /rosout
# let us now run a program
rosrun turtlesim turtlesim_node
# name the node
rosrun turtlesim turtlesim_node __name:=my_turtle
rosnode ping my_turtle
## ROS Topics
roscore
# In a new terminal...
rosrun turtlesim turtlesim_node
# In a new terminal...
rosrun turtlesim turtle_teleop_key
# Install rqt_graph
rosrun rqt_graph rqt_graph
# List topics
rostopic -h
rostopic list -h
rostopic list -v # verbose listings
rostopic type /turtle1/cmd_vel
# Describe messages
rosmsg show geometry_msgs/Twist
rosmsg show turtlesim/Velocity
# Publishing message to topic on command line
# double dash -- tell the parse that none of the following arguments is an option
# Publishes once
$ rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 1.8]'
# Publishes at a rate of 1 Hz
rostopic pub /turtle1/cmd_vel geometry_msgs/Twist -r 1 -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, -1.8]'
rostopic echo /turtle1/pose
# Looking at the rate of messages
rostopic hz /turtle1/pose
# Graph message data
rosrun rqt_plot rqt_plot
## ROS Services
rosservice list #print information about active services
rosservice call #call the service with the provided tags
rosservice type #print the service type
rosservice find #find services by service type
rosservice uri #print service ROSRPC uri
# call a service
rosservice call /clear
# spwan new turtle
rosservice call /spawn 2 2 0.2 ""
# using rosparam
rosparam set #set parameter
rosparam get #get parameter
rosparam load #load parameters from file
rosparam dump #dump parameters to file
rosparam delete #delete parameter
rosparam list #list parameter names
# set background color
rosparam set /turtlesim/background_r 150
rosservice call /clear
rosparam get /turtlesim/background_g
# get all params
rosparam get /
# dump to file
rosparam dump params.yaml
rosparam load params.yaml copy_turtle
rosparam get /copy_turtle/turtlesim/background_b
## RQT and roslaunch
rosrun rqt_console rqt_console
rosrun rqt_logger_level rqt_logger_level
# publish bad message to trigger Warn message
rostopic pub /turtle1/cmd_vel geometry_msgs/Twist -r 1 -- '{linear: {x: 2.0, y: 0.0, z: 0.0}, angular: {x: 0.0,y: 0.0,z: 0.0}}'
# using roslaunch
roscd beginner_tutorials
roslaunch beginner_tutorials turtlemimic.launch
# send command to both turtles
rostopic pub /turtlesim1/turtle1/cmd_vel geometry_msgs/Twist -r 1 -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, -1.8]'
rqt_graph
## Using rosed
rosed roscpp Logger.msg
# Set default editor to vs code
export EDITOR='code'
## ROS msg
rosmsg show beginner_tutorials/Num
roscp rospy_tutorials AddTwoInts.srv srv/AddTwoInts.srv
# Show the service
rossrv show beginner_tutorials/AddTwoInts
# Getting help
rosmsg -h
rosmsg show -h
## Publish Subscriber Python
rosrun beginner_tutorials listener.py
## Publish Subscriber C++
rosrun beginner_tutorials talker

# Custom Service
rosrun beginner_tutorials add_two_ints_server
rosrun beginner_tutorials add_two_ints_client.py 1 3

# ROS Bag
rosbag record -a
rosbag info $BAGFILE
rosbag play $BAGFILE
rosbag play -r 2 $BAGFILE
rosbag record -O subset /turtle/cmd_vel /turtle1/pos

# Debugging ROS
roswtf # my new best friend
